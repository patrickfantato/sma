import { User } from './../../../../interfaces/user.interface';
import { environment } from './../../../../../environments/environment';
import { Router } from '@angular/router';
import { Component, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements AfterViewInit {

  user$: Observable<User>

  constructor(
    private router: Router,
    private http: HttpClient
  ) { }

  ngAfterViewInit() {}

  logout() {
    this.router.navigate(['login'])
  }

  toggleInfoPane(id) {
    this.http.get(`${environment.apiURL}/quiz/${id}`)
    .subscribe(quiz => {
      document.getElementById('info-pane').innerHTML = quiz.toString()
    })
  }
}
