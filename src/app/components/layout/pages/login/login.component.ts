import { AuthService } from '../../../../services/user/auth.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup
  @ViewChild('emailInput', {static: false}) private emailInput: ElementRef<HTMLInputElement>
  
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService, 
  ) { }

  ngOnInit() {
  }

  login() {
    const matricula = this.loginForm.get('matricula').value
    const password = this.loginForm.get('password').value
    this
      .authService
      .authenticate(matricula, password)
      .subscribe(
        res => {
          if(res.body.auth)
            this.router.navigate(['home'])
        },
        (error) => alert('Usuário ou senha inválidos')
      )
  }
}
