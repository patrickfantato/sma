export interface User {
  _id?: string,
  name: string,
  empresa: string,
  codigo: string,
  password: string,
  matricula: string
}
export default User