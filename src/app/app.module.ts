import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { InlineSVGModule } from 'ng-inline-svg';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './components/layout/shared/map/map.component';
import { SliderComponent } from './components/layout/shared/slider/slider.component';
import { LoginComponent } from './components/layout/pages/login/login.component';
import { MainComponent } from './components/layout/pages/main/main.component';
import { WelcomeComponent } from './components/layout/pages/welcome/welcome.component';
import { ScoreComponent } from './components/layout/pages/score/score.component';
import { QuestionComponent } from './components/layout/shared/question/question.component';

import { CarouselComponent } from './components/layout/shared/carousel/carousel.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    SliderComponent,
    LoginComponent,
    MainComponent,
    ScoreComponent,
    QuestionComponent,
    WelcomeComponent,
    CarouselComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    InlineSVGModule.forRoot(),
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
