import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from './../../../environments/environment';

import { TokenService } from './token.service';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private tokenService: TokenService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.tokenService.hasToken() && request.url.includes(environment.apiURL)) {
            request = request.clone({
                setHeaders: { 
                    Authorization: `Bearer ${this.tokenService.getToken()}`
                }
            });
        }
        return next.handle(request);
    }
}