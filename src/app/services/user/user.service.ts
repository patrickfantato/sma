import { HttpClient } from '@angular/common/http';
import { User } from '../../interfaces/user.interface';
import { TokenService } from './token.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as jwt_decode from 'jwt-decode'
import { environment } from 'src/environments/environment';

const userKey = 'user'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userSubject = new BehaviorSubject<User>(null)

  constructor(
    private http: HttpClient,
    private tokenService: TokenService) { 
      this.tokenService.hasToken() && this.decodeAndNotify()
  }

  setToken(token: string) {
    this.tokenService.setToken(token)
  }

  getUser() {
    return this.userSubject.asObservable()
  }

  getUserById(user: string) {
    return this.http.get<User>(`${environment.apiURL}/users/${user}`)
  }

  setUser(user) {
    window.localStorage.setItem(userKey, user)
    this.userSubject.next(user)
  }

  private decodeAndNotify() {
    const token = this.tokenService.getToken()
    const user = jwt_decode(token).user as User
    this.userSubject.next(user)
  }

  isLogged() {
    return this.tokenService.hasToken()
  }

  logout() {
    this.getUser().subscribe(user => {
      this.http.post(`${environment.apiURL}/logout`, { id: user._id, token: this.tokenService.getToken() })
      .subscribe(status => {
        this.tokenService.removeToken()
        this.userSubject.next(null)
      })
    })
  }

}
