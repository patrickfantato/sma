import { environment } from './../../../environments/environment';
import { UserService } from './user.service';
import { Injectable } from '@angular/core';

import { User } from './../../interfaces/user.interface';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

export interface ResponseAuth {
  auth: boolean,
  user: User,
  token: string,
  error
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private headers = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(
    private http: HttpClient,

    private userService: UserService) { }

  authenticate(email: string, password: string) {
    return this
      .http
      .post<ResponseAuth>(
        `${environment.apiURL}/login`, 
        { user: { email, password } }, 
        { headers: this.headers, observe: 'response' }
      )
      .pipe(tap(res => {
        const authToken = res.body.token;
        this.userService.setToken(authToken);
        if(res.body.auth) {
          this.userService.setUser(res.body.user)
        }
      }))
  }
}
